package lab4.trees;

/**
 * Дерево, представляющее арифметическое выражение с целочисленными операндами.
*/
public class ExpressionTree {
    /** Некоторый элемент арифметического выражения. */
    public abstract static class Expression {
        public abstract int eval();
        public abstract String toString();
    }

    /** Аргумент операции. */
    public static class Number extends Expression {
        protected int value;

        public Number(int value) {
            this.value = value;
        }

        @Override
        public int eval() {
            return value;
        }

        @Override
        public String toString() {
            return Integer.toString(value);
        }
    }

    /** Бинарная операция. */
    public abstract static class Operation extends Expression {
        protected Expression left;
        protected Expression right;

        public Operation(Expression left, Expression right) {
            this.left = left;
            this.right = right;
        }

        public abstract String getSymbol();

        @Override
        public String toString() {
            return String.format("%s %s %s", left.toString(), right.toString(), getSymbol());
        }
    }

    public static class Add extends Operation {
        public Add(Expression left, Expression right) {
            super(left, right);
        }

        @Override
        public int eval() {
            return left.eval() + right.eval();
        }

        @Override
        public String getSymbol() {
            return "+";
        }
    }

    public static class Subtract extends Operation {
        public Subtract(Expression left, Expression right) {
            super(left, right);
        }

        @Override
        public int eval() {
            return left.eval() - right.eval();
        }

        @Override
        public String getSymbol() {
            return "-";
        }
    }

    public static class Multiply extends Operation {
        public Multiply(Expression left, Expression right) {
            super(left, right);
        }

        @Override
        public int eval() {
            return left.eval() * right.eval();
        }

        @Override
        public String getSymbol() {
            return "*";
        }
    }

    public static class Divide extends Operation {
        public Divide(Expression left, Expression right) {
            super(left, right);
        }

        @Override
        public int eval() {
            return left.eval() / right.eval();
        }

        @Override
        public String getSymbol() {
            return "/";
        }
    }

    protected final Expression root;

    public ExpressionTree(Expression root) {
        this.root = root;
    }

    public ExpressionTree(String source) {
        ExpressionParser parser = new ExpressionParser();
        this.root = parser.parse(source);
    }

    /** Возвращает значение выражения. */
    public int eval() {
        return root.eval();
    }

    /**
     * Возвращает строковое представление выражения в обратной польской
     * нотации.
     */
    public String toString() {
        return String.format("%s = %d", root.toString(), eval());
    }
}
