package lab4.trees;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import lab4.trees.ExpressionTree.Add;
import lab4.trees.ExpressionTree.Divide;
import lab4.trees.ExpressionTree.Expression;
import lab4.trees.ExpressionTree.Multiply;
import lab4.trees.ExpressionTree.Number;
import lab4.trees.ExpressionTree.Operation;
import lab4.trees.ExpressionTree.Subtract;

/**
 * Парсер арифметических выражений в обратной польской нотации. Реализован в
 * виде конечного автомата.
 */
public class ExpressionParser {
    public static class ParseException extends RuntimeException {
        private static final long serialVersionUID = -299469709685984709L;

        public ParseException() {
            super();
        }

        public ParseException(String s) {
            super(s);
        }
    }

    /** Одно из состояний, в котором может находиться парсер. */
    protected abstract static class State {
        protected final ExpressionParser parser;

        public State(ExpressionParser parser) {
            this.parser = parser;
        }

        public abstract String getName();
        public void accept(String token) {}
    }

    protected static class WaitState extends State {
        public WaitState(ExpressionParser parser) {
            super(parser);
        }

        @Override
        public String getName() {
            return "wait";
        }
    }

    protected static class DispatchState extends State {
        private static final Map<String, String> patterns = new LinkedHashMap<>();
        static {
            patterns.put("\\d+", "number");
            patterns.put("[+]", "add");
            patterns.put("[-]", "subtract");
            patterns.put("[*]", "multiply");
            patterns.put("[/]", "divide");
        }

        public DispatchState(ExpressionParser parser) {
            super(parser);
        }

        @Override
        public String getName() {
            return "dispatch";
        }

        @Override
        public void accept(String token) {
            for (Map.Entry<String, String> entry : patterns.entrySet())
                if (Pattern.matches(entry.getKey(), token)) {
                    parser.setState(entry.getValue());
                    return;
                }

            throw new ParseException("Неизвестный символ.");
        }
    }

    protected static class NumberState extends State {
        public NumberState(ExpressionParser parser) {
            super(parser);
        }

        @Override
        public String getName() {
            return "number";
        }

        @Override
        public void accept(String token) {
            int value = Integer.parseInt(token);
            parser.stack.push(new Number(value));
            parser.setState("wait");
        }
    }

    protected abstract static class OperationState extends State {
        public OperationState(ExpressionParser parser) {
            super(parser);
        }

        @Override
        public void accept(String token) {
            Expression left, right;

            try {
                right = parser.stack.pop();
                left = parser.stack.pop();
            } catch (NoSuchElementException ex) {
                throw new ParseException("Недостаточно значений в стеке.");
            }
            parser.stack.push(buildOperation(left, right));
            parser.setState("wait");
        }

        public abstract Operation buildOperation(Expression left, Expression right);
    }

    protected static class AddState extends OperationState {
        public AddState(ExpressionParser parser) {
            super(parser);
        }

        @Override
        public String getName() {
            return "add";
        }

        @Override
        public Operation buildOperation(Expression left, Expression right) {
            return new Add(left, right);
        }
    }

    protected static class SubtractState extends OperationState {
        public SubtractState(ExpressionParser parser) {
            super(parser);
        }

        @Override
        public String getName() {
            return "subtract";
        }

        @Override
        public Operation buildOperation(Expression left, Expression right) {
            return new Subtract(left, right);
        }
    }

    protected static class MultiplyState extends OperationState {
        public MultiplyState(ExpressionParser parser) {
            super(parser);
        }

        @Override
        public String getName() {
            return "multiply";
        }

        @Override
        public Operation buildOperation(Expression left, Expression right) {
            return new Multiply(left, right);
        }
    }

    protected static class DivideState extends OperationState {
        public DivideState(ExpressionParser parser) {
            super(parser);
        }

        @Override
        public String getName() {
            return "divide";
        }

        @Override
        public Operation buildOperation(Expression left, Expression right) {
            return new Divide(left, right);
        }
    }

    protected final Map<String, State> states = new HashMap<>();
    protected State currentState;
    protected final Deque<Expression> stack = new ArrayDeque<>();

    public ExpressionParser() {
        Stream.of(
            new WaitState(this),
            new DispatchState(this),
            new NumberState(this),
            new AddState(this),
            new SubtractState(this),
            new MultiplyState(this),
            new DivideState(this)
        ).forEach(this::addState);
    }

    protected void addState(State state) {
        states.put(state.getName(), state);
    }

    protected String getState() {
        return currentState.getName();
    }

    protected void setState(String name) {
        currentState = states.get(name);
    }

    protected void accept(String token) {
        currentState.accept(token);
    }

    public Expression parse(String source) {
        stack.clear();

        Pattern.compile("\\s+").splitAsStream(source).forEach(token -> {
            setState("dispatch");
            while (getState() != "wait")
                accept(token);
        });

        Expression root;
        try {
            root = stack.pop();
        } catch (NoSuchElementException ex) {
            throw new ParseException("Пустая исходная строка.");
        }

        if (!stack.isEmpty())
            throw new ParseException("Слишком много значений.");

        return root;
    }
}
