package lab4;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import lab4.trees.ExpressionTree;

public class Lab4 {
    public static void main(String[] args) throws IOException {
        try (
            Stream<String> exprStream = Files.lines(
                Paths.get("src", "lab4", "expressions.txt")
            );
            PrintWriter writer = new PrintWriter(Files.newBufferedWriter(
                Paths.get("src", "lab4", "results.txt")
            ))
        ) {
            exprStream.forEach(expr -> {
                ExpressionTree exprTree = new ExpressionTree(expr);
                writer.println(exprTree);
            });
        }
    }
}
