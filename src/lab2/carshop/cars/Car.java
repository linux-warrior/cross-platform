package lab2.carshop.cars;

import java.math.BigDecimal;

public abstract class Car {
    public final String color;
    public final int speed;
    public BigDecimal regularPrice;
    public boolean isSoldOut = false;

    public Car(String color, int speed, BigDecimal regularPrice) {
        this.color = color;
        this.speed = speed;
        this.regularPrice = regularPrice;
    }

    public abstract int getDiscount();

    public BigDecimal getSalePrice() {
        return regularPrice.multiply(
            BigDecimal.valueOf(100).subtract(BigDecimal.valueOf(getDiscount()))
        ).divide(
            BigDecimal.valueOf(100)
        );
    }

    public String toString() {
        return String.format("%s[color=%s,isSoldOut=%s]", getClass().getSimpleName(), color, isSoldOut);
    }
}
