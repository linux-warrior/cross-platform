package lab2.carshop.cars;

import java.math.BigDecimal;

public class Sedan extends Car {
    public final int length;

    public Sedan(String color, int speed, BigDecimal regularPrice, int length) {
        super(color, speed, regularPrice);
        this.length = length;
    }

    @Override
    public int getDiscount() {
        return (length > 20) ? 5 : 0;
    }
}
