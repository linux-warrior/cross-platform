package lab2.carshop.cars;

import java.math.BigDecimal;

public class Ford extends Car {
    public final int year;
    public int manufacturerDiscount;

    public Ford(String color, int speed, BigDecimal regularPrice, int year, int manufacturerDiscount) {
        super(color, speed, regularPrice);
        this.year = year;
        this.manufacturerDiscount = manufacturerDiscount;
    }

    @Override
    public int getDiscount() {
        return manufacturerDiscount;
    }
}
