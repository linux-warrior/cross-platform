package lab2.carshop.cars;

import java.math.BigDecimal;

public class Truck extends Car {
    public final int weight;

    public Truck(String color, int speed, BigDecimal regularPrice, int weight) {
        super(color, speed, regularPrice);
        this.weight = weight;
    }

    @Override
    public int getDiscount() {
        return (weight > 2000) ? 10 : 0;
    }
}
