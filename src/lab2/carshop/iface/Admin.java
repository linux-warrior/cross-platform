package lab2.carshop.iface;

import java.math.BigDecimal;

public interface Admin {
    BigDecimal getIncome();
}
