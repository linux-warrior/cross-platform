package lab2.carshop.iface;

import java.math.BigDecimal;
import java.util.stream.Stream;

import lab2.carshop.cars.Car;

public interface Customer {
    Stream<BigDecimal> getCarsPrices();
    Stream<String> getCarsColors();
    BigDecimal getCarPrice(int id);
    String getCarColor(int id);
    Car purchaseCar(int id);
}
