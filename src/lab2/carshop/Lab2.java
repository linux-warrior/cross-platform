package lab2.carshop;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import lab2.carshop.impl.MyOwnAutoShop;

public class Lab2 {
    public static void main(String[] args) {
        MyOwnAutoShop autoShop = new MyOwnAutoShop();
        System.out.println(autoShop);
        System.out.println(
            autoShop.getCarsPrices().map(Object::toString).collect(Collectors.joining(", "))
        );
        System.out.println(autoShop.getCarsColors().collect(Collectors.joining(", ")));

        System.out.println();
        IntStream.of(0, 1, 4).forEach(autoShop::purchaseCar);
        System.out.println(autoShop);
        System.out.println(autoShop.getIncome());
    }
}
