package lab2.carshop.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lab2.carshop.cars.Car;
import lab2.carshop.cars.Ford;
import lab2.carshop.cars.Sedan;
import lab2.carshop.cars.Truck;
import lab2.carshop.iface.Admin;
import lab2.carshop.iface.Customer;

public class MyOwnAutoShop implements Admin, Customer {
    protected final List<Car> inventory;

    public MyOwnAutoShop() {
        this(Arrays.asList(
            new Sedan("Красный", 180, BigDecimal.valueOf(15000), 4),
            new Ford("Синий", 170, BigDecimal.valueOf(12000), 2015, 5),
            new Ford("Серебристый", 160, BigDecimal.valueOf(10000), 2012, 10),
            new Truck("Белый", 140, BigDecimal.valueOf(20000), 1500),
            new Truck("Черный", 120, BigDecimal.valueOf(25000), 3000)
        ));
    }

    public MyOwnAutoShop(List<Car> cars) {
        this.inventory = new ArrayList<>(cars);
    }

    @Override
    public Stream<BigDecimal> getCarsPrices() {
        return inventory.stream().map(Car::getSalePrice);
    }

    @Override
    public Stream<String> getCarsColors() {
        return inventory.stream().map(car -> car.color);
    }

    protected Car getCar(int id) {
        return inventory.get(id);
    }

    @Override
    public BigDecimal getCarPrice(int id) {
        return getCar(id).getSalePrice();
    }

    @Override
    public String getCarColor(int id) {
        return getCar(id).color;
    }

    @Override
    public Car purchaseCar(int id) {
        Car car = getCar(id);
        if (!car.isSoldOut) {
            car.isSoldOut = true;
            return car;
        } else
            throw new IllegalStateException("Эта машина уже продана.");
    }

    @Override
    public BigDecimal getIncome() {
        return inventory.stream().filter(car -> car.isSoldOut).reduce(
            BigDecimal.ZERO,
            (total, car) -> total.add(car.getSalePrice()),
            BigDecimal::add
        );
    }

    public String toString() {
        return inventory.stream().map(Object::toString).collect(Collectors.joining(", "));
    }
}
