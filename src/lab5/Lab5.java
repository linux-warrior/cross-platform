package lab5;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Lab5 {
    public static class Point {
        public int x;
        public int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getManhattanDistance() {
            return Math.abs(x) + Math.abs(y);
        }

        public String toString() {
            return String.format("(%d, %d) - %d", x, y, getManhattanDistance());
        }
    }

    public static class PointComparator implements Comparator<Point> {
        @Override
        public int compare(Point point1, Point point2) {
            int result = Integer.compare(
                point1.getManhattanDistance(),
                point2.getManhattanDistance()
            );
            if (result != 0)
                return result;

            result = Integer.compare(point1.x, point2.x);
            if (result != 0)
                return result;

            return Integer.compare(point1.y, point2.y);
        }
    }

    public static void main(String[] args) {
        List<Point> pointsList = new ArrayList<>();

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Введите количество точек в списке: ");
            int numPoints = scanner.nextInt();
            if (numPoints <= 0) {
                System.out.println("Задано неправильное количество точек.");
                System.exit(1);
            }

            for (int i = 0; i < numPoints; i++) {
                System.out.printf("Введите координаты точки[%d]: ", i);
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                pointsList.add(new Point(x, y));
            }
        }

        pointsList.sort(new PointComparator());

        System.out.println("Отсортированный список точек:");
        {
            int i = 0;
            for (Point point : pointsList)
                System.out.printf("Точка[%d]: %s\n", i++, point);
        }
    }
}
