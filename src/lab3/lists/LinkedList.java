package lab3.lists;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

/** Односвязный линейный список. */
public class LinkedList<T> implements Iterable<T> {
    /** Элемент списка. */
    protected static class Node<T> {
        protected T data;
        protected Node<T> next;

        public Node(T data) {
            this(data, null);
        }

        public Node(T data, Node<T> next) {
            this.data = data;
            this.next = next;
        }
    }

    /** Итератор, возвращающий элементы списка. */
    protected static class Iter<T> implements Iterator<T> {
        protected Node<T> current;

        public Iter(Node<T> node) {
            current = node;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            if (current == null)
                throw new NoSuchElementException();

            T data = current.data;
            current = current.next;
            return data;
        }
    }

    /** Адаптер для итератора, сохраняющий последний извлеченный элемент. */
    protected static class CachedIter<T> implements Iterator<T> {
        protected final Iterator<T> iter;
        protected boolean hasCache = false;
        protected T cache;

        public CachedIter(Iterator<T> iter) {
            this.iter = iter;
        }

        @Override
        public boolean hasNext() {
            return hasCache || iter.hasNext();
        }

        @Override
        public T next() {
            if (hasCache) {
                hasCache = false;
                return cache;
            } else
                return iter.next();
        }

        public T peek() {
            if (!hasCache) {
                cache = iter.next();
                hasCache = true;
            }
            return cache;
        }
    }

    /** Итератор, выполняющий слияние двух последовательностей. */
    protected static class MergeIter<T extends Comparable<? super T>> implements Iterator<T> {
        protected final CachedIter<? extends T> iter1;
        protected final CachedIter<? extends T> iter2;

        public MergeIter(Iterator<? extends T> iter1, Iterator<? extends T> iter2) {
            this.iter1 = new CachedIter<>(iter1);
            this.iter2 = new CachedIter<>(iter2);
        }

        public MergeIter(Iterable<? extends T> seq1, Iterable<? extends T> seq2) {
            this(seq1.iterator(), seq2.iterator());
        }

        @Override
        public boolean hasNext() {
            return iter1.hasNext() || iter2.hasNext();
        }

        @Override
        public T next() {
            if (iter1.hasNext() && iter2.hasNext()) {
                if (iter1.peek().compareTo(iter2.peek()) < 0)
                    return iter1.next();
                else
                    return iter2.next();
            }

            if (iter1.hasNext()) {
                return iter1.next();
            } else {
                return iter2.next();
            }
        }
    }

    protected Node<T> head;
    protected Node<T> tail;

    public LinkedList() {}

    public LinkedList(Iterable<? extends T> seq) {
        this();
        addAll(seq);
    }

    @SafeVarargs
    public LinkedList(T... seq) {
        this(Arrays.asList(seq));
    }

    /** Добавляет элемент в конец списка. */
    public void add(T data) {
        Node<T> node = new Node<>(data);
        if (tail == null) {
            head = tail = node;
        } else {
            tail.next = node;
            tail = tail.next;
        }
    }

    /** Добавляет в конец списка все элементы последовательности. */
    public void addAll(Iterable<? extends T> seq) {
        for (T data : seq)
            add(data);
    }

    /** Удаляет первый элемент списка и возвращает его. */
    public T remove() {
        Iter<T> iter = new Iter<>(head);
        T data = iter.next();
        // Немного помогаем сборщику мусора.
        head.data = null;
        head.next = null;
        head = iter.current;
        if (head == null)
            tail = null;

        return data;
    }

    /** Удаляет все элементы списка. */
    public void clear() {
        while (head != null)
            remove();
    }

    /** Возвращает итератор на элементы списка. */
    @Override
    public Iterator<T> iterator() {
        return new Iter<>(head);
    }

    /**
     * Выполняет слияние двух последовательностей, отсортированных по
     * возрастанию значений элементов.
     */
    public static <T extends Comparable<? super T>> Iterable<T> merge(
            Iterable<? extends T> seq1, Iterable<? extends T> seq2) {
        return () -> new MergeIter<>(seq1, seq2);
    }

    /**
     * Класс, выполняющий рекурсивную обработку заданной последовательности.
     */
    protected static class Traverser<T> {
        protected final Consumer<? super T> consumer;
        protected final Iterator<? extends T> iter;

        public Traverser(Consumer<? super T> consumer, Iterator<? extends T> iter) {
            this.consumer = consumer;
            this.iter = iter;
        }

        public Traverser(Consumer<? super T> consumer, Iterable<? extends T> seq) {
            this(consumer, seq.iterator());
        }

        public void traverse() {
            if (!iter.hasNext())
                return;
            consumer.accept(iter.next());
            traverse();
        }
    }

    /**
     * Выполняет рекурсивный проход по списку, передавая значения элементов в
     * {@code consumer}.
     */
    public void traverse(Consumer<? super T> consumer) {
        traverse(consumer, this);
    }

    /**
     * Выполняет рекурсивный проход по последовательности {@code seq},
     * передавая значения элементов в {@code consumer}.
     */
    public static <T> void traverse(Consumer<? super T> consumer, Iterable<? extends T> seq) {
        Traverser<T> traverser = new Traverser<>(consumer, seq);
        traverser.traverse();
    }

    /**
     * Выполняет слияние двух последовательностей, отсортированных по
     * возрастанию значений элементов, передавая значения в {@code consumer}.
     */
    public static <T extends Comparable<? super T>> void mergeTraverse(
            Consumer<? super T> consumer, Iterable<? extends T> seq1, Iterable<? extends T> seq2) {
        traverse(consumer, merge(seq1, seq2));
    }
}
