package lab3;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import lab3.lists.LinkedList;

public class Lab3 {
    public static void main(String[] args) {
        LinkedList<Integer> list1 = new LinkedList<>(1, 3, 5, 7, 9, 10);
        LinkedList<Integer> list2 = new LinkedList<>(2, 4, 6, 8);
        Iterable<Integer> merge = LinkedList.merge(list1, list2);
        System.out.println(
            StreamSupport.stream(
                merge.spliterator(), false
            ).map(
                Object::toString
            ).collect(
                Collectors.joining(", ")
            )
        );
        LinkedList.mergeTraverse(data -> System.out.println(data), list1, list2);
        list1.clear();
        list2.clear();
    }
}
