package lab1.playingcards;

import java.util.stream.Stream;

/** Масть карты. */
public class Suit extends Param {
    public static class Manager extends Param.Manager<Suit> {
        public Manager() {
            super();
        }

        public Manager(Stream<? extends Suit> params) {
            super(params);
        }
    }

    public static final Manager manager = new Manager();

    public static final Suit CLUBS = new Suit(1, "крестей");
    public static final Suit DIAMONDS = new Suit(2, "бубен");
    public static final Suit HEARTS = new Suit(3, "червей");
    public static final Suit SPADES = new Suit(4, "пик");

    protected Suit(int id, String name) {
        super(id, name);
    }
}
