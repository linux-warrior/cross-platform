package lab1.playingcards;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Абстрактный класс, описывающий некий параметр, характеризующийся уникальным
 * целочисленным идентификатором.
 */
public abstract class Param {
    public static class InvalidChoiceException extends RuntimeException {
        private static final long serialVersionUID = 1185378500924852052L;

        public InvalidChoiceException() {
            super();
        }

        public InvalidChoiceException(String s) {
            super(s);
        }
    }

    /** Абстрактная фабрика, хранящая все допустимые значения параметра. */
    public static class Manager<T extends Param> {
        protected final Map<Integer, T> choices;

        public Manager() {
            this(Stream.empty());
        }

        public Manager(Stream<? extends T> params) {
            this.choices = params.collect(
                Collectors.toMap(
                    T::getId,
                    Function.identity(),
                    (existingId, newId) -> newId,
                    LinkedHashMap::new
                )
            );
        }

        public Map<Integer, T> getChoices() {
            return Collections.unmodifiableMap(choices);
        }

        public boolean contains(int id) {
            return choices.containsKey(id);
        }

        public T get(int id) {
            T param = choices.get(id);
            if (param == null)
                throw new InvalidChoiceException(String.format("Задан неверный идентификатор параметра - %d.", id));

            return param;
        }

        public Stream<T> all() {
            return choices.values().stream();
        }
    }

    public static final Manager<Param> manager = new Manager<>();

    protected final int id;
    protected final String name;

    protected Param(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Param param = (Param)other;
        return id == param.id;
    }

    public int hashCode() {
        return Objects.hash(id);
    }
}
