package lab1.playingcards;

import java.util.stream.Stream;

/** Ранг карты из колоды для игры в преферанс. */
public class PreferansRank extends Rank {
    public static final Manager manager = new Manager(
        Stream.concat(
            Rank.createPipRange(7, 10),
            Stream.of(Rank.JACK, Rank.QUEEN, Rank.KING, Rank.ACE)
        )
    );

    protected PreferansRank(int id, String name) {
        super(id, name);
    }
}
