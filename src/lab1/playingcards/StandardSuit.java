package lab1.playingcards;

import java.util.stream.Stream;

/** Масть карты из стандартной колоды. */
public class StandardSuit extends Suit {
    public static final Manager manager = new Manager(
        Stream.of(Suit.CLUBS, Suit.DIAMONDS, Suit.HEARTS, Suit.SPADES)
    );

    protected StandardSuit(int id, String name) {
        super(id, name);
    }
}
