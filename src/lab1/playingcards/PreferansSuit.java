package lab1.playingcards;

import java.util.stream.Stream;

/** Масть карты из колоды для игры в преферанс. */
public class PreferansSuit extends Suit {
    public static final Manager manager = new Manager(
        Stream.of(Suit.SPADES, Suit.CLUBS, Suit.DIAMONDS, Suit.HEARTS)
    );

    protected PreferansSuit(int id, String name) {
        super(id, name);
    }
}
