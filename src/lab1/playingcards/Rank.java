package lab1.playingcards;

import java.util.stream.IntStream;
import java.util.stream.Stream;

/** Ранг карты. */
public class Rank extends Param {
    public static class Manager extends Param.Manager<Rank> {
        public Manager() {
            super();
        }

        public Manager(Stream<? extends Rank> params) {
            super(params);
        }
    }

    public static final Manager manager = new Manager();

    public static final Rank ACE = new Rank(1, "Туз");
    public static final Rank JACK = new Rank(11, "Валет");
    public static final Rank QUEEN = new Rank(12, "Дама");
    public static final Rank KING = new Rank(13, "Король");

    protected Rank(int id, String name) {
        super(id, name);
    }

    /** Создает фоску. */
    protected static Rank createPip(int value) {
        return new Rank(value, Integer.toString(value));
    }

    /** Создает диапазон фосок от {@code low} до {@code high} */
    protected static Stream<Rank> createPipRange(int low, int high) {
        return IntStream.rangeClosed(low, high).mapToObj(Rank::createPip);
    }
}
