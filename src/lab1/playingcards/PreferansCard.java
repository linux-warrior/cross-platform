package lab1.playingcards;

/** Карта из колоды для игры в преферанс. */
public class PreferansCard extends Card {
    public static final Manager manager = new Manager(PreferansRank.manager, PreferansSuit.manager);

    public PreferansCard(Rank rank, Suit suit) {
        super(rank, suit);
    }
}
