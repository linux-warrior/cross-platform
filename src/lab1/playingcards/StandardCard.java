package lab1.playingcards;

/** Карта из стандартной колоды. */
public class StandardCard extends Card {
    public static final Manager manager = new Manager(StandardRank.manager, StandardSuit.manager);

    public StandardCard(Rank rank, Suit suit) {
        super(rank, suit);
    }
}
