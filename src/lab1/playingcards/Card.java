package lab1.playingcards;

import java.util.Objects;
import java.util.stream.Stream;

/** Игральная карта. */
public class Card {
    public static class Manager {
        protected final Rank.Manager rankManager;
        protected final Suit.Manager suitManager;

        public Manager(Rank.Manager rankManager, Suit.Manager suitManager) {
            this.rankManager = rankManager;
            this.suitManager = suitManager;
        }

        public Card get(int rankId, int suitId) {
            Rank rank = rankManager.get(rankId);
            Suit suit = suitManager.get(suitId);
            return new Card(rank, suit);
        }

        /**
         * Возвращает список всех возможных карт, отсортированный по масти и
         * рангу.
         */
        public Stream<Card> all() {
            return suitManager.all().flatMap(
                suit -> rankManager.all().map(
                    rank -> new Card(rank, suit)
                )
            );
        }
    }

    public static final Manager manager = new Manager(Rank.manager, Suit.manager);

    protected final Rank rank;
    protected final Suit suit;

    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public String toString() {
        return String.format("%s %s", rank, suit);
    }

    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Card card = (Card)other;
        return Objects.equals(rank, card.rank) && Objects.equals(suit, card.suit);
    }

    public int hashCode() {
        return Objects.hash(rank, suit);
    }
}
