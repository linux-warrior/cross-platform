package lab1.playingcards;

import java.util.stream.Stream;

/** Ранг карты из стандартной колоды. */
public class StandardRank extends Rank {
    public static final Manager manager = new Manager(
        Stream.concat(
            Rank.createPipRange(2, 10),
            Stream.of(Rank.JACK, Rank.QUEEN, Rank.KING, Rank.ACE)
        )
    );

    protected StandardRank(int id, String name) {
        super(id, name);
    }
}
