package lab1;

import lab1.deck.Deck;
import lab1.geometry.Rectangle;
import lab1.playingcards.PreferansCard;

public class Lab1 {
    public static void main(String[] args) {
        Deck deck = new Deck(PreferansCard.manager);
        deck.shuffle();

        while (!deck.isEmpty())
            System.out.println(deck.deal());

        System.out.println();

        Rectangle rect = new Rectangle(10, 10, 30, 20);
        System.out.println(rect);
        System.out.printf("%.2f x %.2f = %.2f\n", rect.getWidth(), rect.getHeight(), rect.getArea());
    }
}
