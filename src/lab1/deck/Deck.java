package lab1.deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import lab1.playingcards.Card;

/** Колода карт. */
public class Deck {
    protected final Card.Manager cardManager;
    protected final List<Card> cards;

    public Deck(Card.Manager cardManager) {
        this.cardManager = cardManager;
        this.cards = cardManager.all().collect(Collectors.toCollection(ArrayList::new));
    }

    public List<Card> getCards() {
        return Collections.unmodifiableList(cards);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    public int size() {
        return cards.size();
    }

    /** Тасует колоду. */
    public void shuffle() {
        Collections.shuffle(cards);
    }

    /** Возвращает карту из колоды. */
    public Card deal() {
        return cards.remove(0);
    }
}
