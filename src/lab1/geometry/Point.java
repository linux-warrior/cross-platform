package lab1.geometry;

import java.util.Objects;

public class Point {
    public double x;
    public double y;

    public Point() {
        this(0, 0);
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point point) {
        this(point.x, point.y);
    }

    public Point offset(double offsetX, double offsetY) {
        return new Point(x + offsetX, y + offsetY);
    }

    public String toString() {
        return String.format("(%.2f, %.2f)", x, y);
    }

    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Point point = (Point)other;
        return x == point.x && y == point.y;
    }

    public int hashCode() {
        return Objects.hash(x, y);
    }
}
