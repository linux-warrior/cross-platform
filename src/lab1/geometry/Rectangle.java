package lab1.geometry;

import java.util.Objects;

public class Rectangle {
    protected Point topLeft;
    protected Point bottomRight;

    public Rectangle(Point topLeft, Point bottomRight) {
        this.topLeft = new Point(topLeft);
        this.bottomRight = new Point(bottomRight);
    }

    public Rectangle(double left, double top, double right, double bottom) {
        this(new Point(left, top), new Point(right, bottom));
    }

    public Rectangle(double width, double height) {
        this(new Point(), width, height);
    }

    public Rectangle(Point topLeft, double width, double height) {
        this(topLeft, topLeft.offset(width, height));
    }

    public Rectangle(Rectangle rect) {
        this(rect.topLeft, rect.bottomRight);
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public void setBottomRight(Point bottomRight) {
        this.bottomRight = bottomRight;
    }

    public Point getTopRight() {
        return new Point(bottomRight.x, topLeft.y);
    }

    public Point getBottomLeft() {
        return new Point(topLeft.x, bottomRight.y);
    }

    public double getWidth() {
        return bottomRight.x - topLeft.x;
    }

    public double getHeight() {
        return bottomRight.y - topLeft.y;
    }

    public double getArea() {
        return getWidth() * getHeight();
    }

    public String toString() {
        return String.format("(%s, %s)", topLeft, bottomRight);
    }

    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Rectangle rect = (Rectangle)other;
        return Objects.equals(topLeft, rect.topLeft) && Objects.equals(bottomRight, rect.bottomRight);
    }

    public int hashCode() {
        return Objects.hash(topLeft, bottomRight);
    }
}
